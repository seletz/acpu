import sys

from py65.memory import ObservableMemory

from . import console


def putc(address, value):
    sys.stdout.write(chr(value))

def getc(address):
    c = console.getch(sys.stdin)
    return ord(c)

PUTC = 0xf1
GETC = 0xf0


def make_memory():
    memory = ObservableMemory()

    memory.subscribe_to_read([GETC], getc)
    memory.subscribe_to_write([PUTC], putc)

    return memory
