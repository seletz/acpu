from . import memory
from . import cpu


def run(cpu):
    while not cpu.isHalted():
        cpu.step()

    print
    print str(cpu)
    print "CPU halted."

def singlestep(cpu):
    quit = False
    while not cpu.isHalted() and not quit:
        m = cpu.mem[cpu.PC]
        print "$%04x: $%02x (%3d %s)" % (cpu.PC, m, m, "%{:08b}".format(m))

        print "     " + str(cpu)
        cpu.step()

        k = raw_input("-->")
        quit = k.lower() == "q"

    print str(cpu)
    print "CPU halted."

if __name__ == "__main__":
    mem = memory.make_memory()

    mcpu = cpu.CPU(mem)


    mem.write(0x00, [
        0x01, 0xc0,             # 00 LDA $c0
        0x02, 0xf1,             # 02 STA $f1  (PUTC)
        0x03, 0x01,             # 04 ADD 1
        0x02, 0xc0,             # 06 STA $c0
        0x04, 0x3a,             # 08 CMP $3a
        0x05, 0x0e,             # 0a BEQ $0e
        0x06, 0x02,             # 0c JMP $02
        0xff                    # 0e HLT
    ])

    mem[0xc0] = 0x30

    run(mcpu)
    # singlestep(mcpu)
