import sys
import struct


def signedByte(v):
    return struct.unpack("b", chr(v & 0xff))[0]


class CPU:
    """
    Simple "CPU".

    Single address machine, one register. No stack.  At reset, starts
    executing from $00.

    Registers
    ---------

    PC          program counter
    A           accumulator
    P           processor flags


    Opcodes
    -------

    NOP         $00
    LDA $adr    $01 NN
    STA $adr    $02 NN
    ADD #imm    $03 NN
    CMP #imm    $04 NN
    BEQ $abs    $05 NN
    JMP $adr    $06 NN
    HLT         $ff


    """
    FLAG_HLT    = 3
    FLAG_ZERO   = 0
    FLAG_NEG    = 7

    def __init__(self, mem):
        self.mem = mem
        self.reset()

    def __str__(self):
        k = "PC %04x A %02x Flags: " % (self.PC, self.A)
        if self.isNegative():
            k += "N "
        if self.isZero():
            k += "Z "
        if self.isHalted():
            k += "H "

        return k

    def step(self):
        if self.isHalted():
            raise RuntimeError("CPU Halted.")

        self.fetch()
        self.decode()
        self.execute()



    def reset(self):
        self.PC = 0
        self.P  = [False] * 8
        self.A  = 0

    def fetch(self):
        self.opcode = self.mem[self.PC]
        self.incPC()

    def decode(self):
        opcodes = {
            0x00: "NOP",
            0x01: "LDA",
            0x02: "STA",
            0x03: "ADD",
            0x04: "CMP",
            0x05: "BEQ",
            0x06: "JMP",
            0xff: "HLT",
        }

        if self.opcode not in opcodes:
            self.opcode = 0xff

        self.instruction = opcodes.get(self.opcode)

    def execute(self):
        handler = getattr(self, "execute_%s" % self.instruction, self.execute_HLT)
        handler()

    def incPC(self):
        self.PC = (self.PC + 1) % 0xff

    def getNextByte(self):
        b = self.mem[self.PC]
        self.incPC()
        return b

    def setFlags(self):
        self.P[self.FLAG_ZERO] = self.A == 0
        self.P[self.FLAG_NEG] =  self.A < 0

    def isZero(self):
        return self.P[self.FLAG_ZERO]

    def isNegative(self):
        return self.P[self.FLAG_NEG]

    def isHalted(self):
        return self.P[self.FLAG_HLT]

    def execute_NOP(self):
        return

    def execute_HLT(self):
        self.P[self.FLAG_HLT] = True

    def execute_LDA(self):
        adr = self.getNextByte()
        self.A = self.mem[adr]
        self.setFlags()

    def execute_STA(self):
        adr = self.getNextByte()
        self.mem[adr] = self.A

    def execute_ADD(self):
        val = signedByte(self.getNextByte())
        self.A = signedByte(signedByte(self.A) + val)
        self.setFlags()

    def execute_CMP(self):
        val = signedByte(self.getNextByte())
        saved = self.A
        self.A = signedByte(signedByte(self.A) - val)
        self.setFlags()
        self.A = saved

    def execute_JMP(self):
        adr = self.getNextByte()
        self.PC = adr & 0xff

    def execute_BEQ(self):
        adr = self.getNextByte()
        if self.isZero():
            self.PC = adr & 0xff
