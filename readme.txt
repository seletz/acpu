acpu
====

Simple "CPU".

Single address machine, one register. No stack.  At reset, starts
executing from $00.

8bit, 256 bytes addressable.

Registers
---------

PC          program counter
A           accumulator
P           processor flags


Opcodes
-------

NOP         $00
LDA $adr    $01 NN
STA $adr    $02 NN
ADD #imm    $03 NN
CMP #imm    $04 NN
BEQ $abs    $05 NN
JMP $adr    $06 NN
HLT         $ff

Usage
-----

First, import the package:

>>> import acpu

Let's create some memory for our CPU:

>>> from acpu import memory
>>> mem = memory.make_memory()

Now construct a CPU instance, giving it the memory:

>>> from acpu import cpu
>>> mcpu = cpu.CPU(mem)

Let's load some instructions into the memory:

>>> mem.write(0x00, [0x01, 0xc0, 0x03, 0x01, 0x04, 0x0a, 0x05, 0x0a, 0x06, 0x02, 0xff])

And some values to work with:

>>> mem[0xc0] = 0x00

Now lad the monitor and run the CPU until itÄs halted:

>>> from acpu import monitor
>>> monitor.run(mcpu)

Or singlestep it:

>>> mcpu.reset()
>>> monitor.singlestep(mcpu)
